package org.example;
public class ValidationMotDePasse {
    private String motDePasse;
    public ValidationMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
    public boolean aUneLongueurAcceptable() {
        return motDePasse.length() >= 8 && motDePasse.length() <= 20;
    }

    public boolean contientLettreMajuscule() {
        for (char c : motDePasse.toCharArray()) {
            if (Character.isUpperCase(c)) {
                return true;
            }
        }
        return false;
    }
    public boolean contientLettreMinuscule() {
        for (char c : motDePasse.toCharArray()) {
            if (Character.isLowerCase(c)) {
                return true;
            }
        }
        return false;
    }
    public boolean contientChiffre() {
        for (char c : motDePasse.toCharArray()) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }
}