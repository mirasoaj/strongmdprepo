package org.example;
import java.lang.System;

public class App {
    public static void main(String[] args) {

        ValidationMotDePasse mdp1 = new ValidationMotDePasse("pwd");

        boolean longueurMinimaleNonRespectee = !mdp1.aUneLongueurAcceptable();
        boolean longueurMaximaleNonRespectee = !mdp1.aUneLongueurAcceptable();

        if (longueurMinimaleNonRespectee) {
            System.out.println("La longueur minimale du mot de passe n'est pas respectée.");
        } else {
            System.out.println("La longueur minimale du mot de passe est respectée.");
        }

        mdp1 = new ValidationMotDePasse("ouinfbykbe");

        mdp1 = new ValidationMotDePasse("ouinfikbeiuhiuhfbosjfizbnujybsjdozihfjbfiue");
        if (longueurMaximaleNonRespectee) {
            System.out.println("La longueur minimale du mot de passe n'est pas respectée.");
        } else {
            System.out.println("La longueur minimale du mot de passe est respectée.");
        }
    }
}
