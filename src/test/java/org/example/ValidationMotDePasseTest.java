package org.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ValidationMotDePasseTest {

    @Test
    public void testLongueurAcceptable() {
        ValidationMotDePasse mdp1 = new ValidationMotDePasse("Password1");
        assertTrue(mdp1.aUneLongueurAcceptable());

        ValidationMotDePasse mdp2 = new ValidationMotDePasse("pass");
        assertFalse(mdp2.aUneLongueurAcceptable());

        ValidationMotDePasse mdp3 = new ValidationMotDePasse("ThisIsALongPasswordThatIsMoreThan20Characters");
        assertFalse(mdp3.aUneLongueurAcceptable());

        ValidationMotDePasse mdp4 = new ValidationMotDePasse("aB1cdEiu");
        assertTrue(mdp4.aUneLongueurAcceptable());

    }

    @Test
    public void testContientLettreMajuscule() {
        ValidationMotDePasse mdp1 = new ValidationMotDePasse("Password1");
        assertTrue(mdp1.contientLettreMajuscule());

        ValidationMotDePasse mdp2 = new ValidationMotDePasse("password1");
        assertFalse(mdp2.contientLettreMajuscule());
    }

    @Test
    public void testContientLettreMinuscule() {
        ValidationMotDePasse mdp1 = new ValidationMotDePasse("Password1");
        assertTrue(mdp1.contientLettreMinuscule());

        ValidationMotDePasse mdp2 = new ValidationMotDePasse("PASSWORD1");
        assertFalse(mdp2.contientLettreMinuscule());
    }

    @Test
    public void testContientChiffre() {
        ValidationMotDePasse mdp1 = new ValidationMotDePasse("Password1");
        assertTrue(mdp1.contientChiffre());

        ValidationMotDePasse mdp2 = new ValidationMotDePasse("Password");
        assertFalse(mdp2.contientChiffre());
    }
}
